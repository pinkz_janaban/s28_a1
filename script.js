//GET all todos
fetch('https://jsonplaceholder.typicode.com/todos')
.then ((response) => response.json())
.then ((data) => {
	let list = data.map((todo)=>{
		return todo.title;
	})
	console.log(list)
})


//Getting a specific to do list item
fetch('https://jsonplaceholder.typicode.com/todos/22')
.then ((response) => response.json())
.then ((data) => console.log(`The item ${data.title} has a status of ${data.completed}`))

//Creating a to do list item using POST method
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body:JSON.stringify({
	  	title: 'Created To Do List Item',
	  	completed: false,
	  	userId: 1
	})

})
.then((response) => response.json())
.then ((data) => console.log(data));


//Creating a to do list item using PUT method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body:JSON.stringify({
	  	title: 'Updated To Do List Item',
	  	description: 'To update the my to do list with a different data structure',
	  	status: 'Pending',
	  	datacompleted: 'Pending',
	  	userId: 1
	})

})
.then((response) => response.json())
.then ((data) => console.log(data));


//Updating a to do list item using Patch method
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body:JSON.stringify({
		status: 'complete',
		datacompleted: '01/19/22'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

//Deleting a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'Delete'
})

.then((response) => response.json())
.then((json) => console.log(json))
